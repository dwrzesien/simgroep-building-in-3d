var TerrainMesh = {
    LoadMeshToScene: function(scene, file, width, height) {
        var img = new Image();
        var me = this;
        img.onload = function() {
            var plane = new THREE.PlaneGeometry(width, height, width - 1, height - 1);
            var data = me.getHeightData(img, width, height);

            for (var y = 0; y < data.length; y++) {
                plane.vertices[y].z = data[y] / 10;
            }
            //Create Material for terrain
            var material = new THREE.MeshLambertMaterial({map: me.createTexture('img/grass.jpg', 5)});
            //Create Terrarain
            var terrain = new THREE.Mesh(plane, material);
            terrain.rotation.x = -Math.PI / 2;
            terrain.receiveShadow = true;
            scene.add(terrain);
            //Create path on the ground
            var path = new THREE.Mesh(
                    new THREE.PlaneGeometry(width, height, 10, 10),
                    new THREE.MeshBasicMaterial({map: me.createTexture("img/path.jpg", 10)})
                    );
            path.position.y = 2;
            path.rotation.x = -Math.PI / 2;
            scene.add(path);
        }
        img.src = file;
    },
    createTexture: function(file, repeat) {
        var texture = new THREE.ImageUtils.loadTexture(file);
        texture.repeat.set(repeat, repeat);
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        return texture;
    },
    getHeightData: function(img, x, y) {
        var canvas = document.createElement('canvas');
        canvas.width = x;
        canvas.height = y;
        var context = canvas.getContext('2d');

        var size = x * y, data = new Float32Array(size);

        context.drawImage(img, 0, 0);

        for (var i = 0; i < size; i++) {
            data[i] = 0;
        }

        var imgd = context.getImageData(0, 0, x, y);
        var pix = imgd.data;

        var j = 0;
        for (var i = 0, n = pix.length; i < n; i += (4)) {
            var all = pix[i] + pix[i + 1] + pix[i + 2];
            data[j++] = all / 30;
        }

        return data;
    }
};