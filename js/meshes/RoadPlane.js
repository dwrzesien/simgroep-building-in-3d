var RoadPlane = {
    LoadRoadToScene: function(scene, file, pos_x, pos_y, pos_z, width, height) {
        var plane = new THREE.PlaneGeometry(width, height, width - 1, height - 1);
        var texture = new THREE.ImageUtils.loadTexture(file);
        texture.repeat.set(1, 10);
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;

        var object = new THREE.Mesh(plane, new THREE.MeshBasicMaterial({map: texture}));
        object.position.x = pos_x;
        object.position.y = pos_y;
        object.position.z = pos_z;
        object.rotation.x = -Math.PI / 2;
        object.rotation.z = -Math.PI / 2;
        scene.add(object);
    }
};