/**
 * Created by richard on 4/10/14.
 */

var IndexController = {
    init: function() {
        //Create Camera
        camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
        //Create Scene
        scene = new THREE.Scene();
        controls = new THREE.PointerLockControls(camera);
        scene.add(controls.getObject());

        ray = new THREE.Raycaster();
        ray.ray.direction.set(0, -1, 0);

        //Load Terrain Mesh
        TerrainMesh.LoadMeshToScene(scene, 'img/terrain.bmp', 300, 300);
        //Load Parking Mesh
        ParkingPlane.LoadParkingToScene(scene, 'img/parking.jpg', 40, 3, -10, 60, 30);
        //Load Road Mesh
        RoadPlane.LoadRoadToScene(scene, 'img/road.jpg', 0, 3, 40, 30, 300);
        // Load the background texture
        var geometry = new THREE.SphereGeometry(500, 60, 40);
        geometry.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));
        var material = new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture('img/sky7.jpg')
        });
        mesh = new THREE.Mesh(geometry, material);
        scene.add(mesh);

        //Create Renderer
        renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        renderer.setClearColor(0xffffff);
        renderer.setSize(window.innerWidth, window.innerHeight);

        renderer.shadowMapEnabled = true;
        renderer.shadowMapCullFace = THREE.CullFaceBack;
        document.body.appendChild(renderer.domElement);

        //Resize Window
        window.addEventListener('resize', onWindowResize, false);

    },
    goTowardsTargetPoint: function(target) {
        controls.goTowardsInitPosition();
        camera.position.x += (target.x - camera.position.x) / targetInertion;
        camera.position.y += (target.y - camera.position.y) / targetInertion;
        camera.position.z += (target.z - camera.position.z) / targetInertion;

        camera.rotation.x *= 0.7;
        camera.rotation.y += (target.ry - camera.rotation.y) / targetInertion;
        camera.rotation.z *= 0.7;

        if (
                Math.abs(target.x - camera.position.x) < 2
                && Math.abs(target.y - camera.position.y) < 2
                && Math.abs(target.z - camera.position.z) < 2
                ) {
            IndexController.clearTarget();
        }
    },
    animate: function() {
        requestAnimationFrame(IndexController.animate);
        controls.update(Date.now() - time);

        if (undefined != this.targetPoint) {
            IndexController.goTowardsTargetPoint(targetPoint);
        }

        renderer.render(scene, camera);
        time = Date.now();

//        if (objects['cube'] != undefined) {
//            objects['cube'].rotation.x += 0.01;
//            objects['cube'].rotation.y += 0.05;
//        }
//
//        rotationAngle += 0.01;
//        if (objects['sphere'] != undefined) {
//            objects['sphere'].position.x = Math.sin(rotationAngle) * sphereOrbitRadius;
//            objects['sphere'].position.z = Math.cos(rotationAngle) * sphereOrbitRadius - 30;
//        }

    },
    chooseTargetPoint: function(pointIndex) {
        targetPoint = targetPoints[pointIndex];
    },
    clearTarget: function() {
        targetPoint = null;
    },
    loadObj: function(obj, mtl, posX, posY, posZ) {
        var loader = new THREE.OBJMTLLoader();
        loader.load(obj, mtl, function(object) {
            object.position.x = posX;
            object.position.y = posY;
            object.position.z = posZ;

            object.castShadow = true;
            object.receiveShadow = true;
            scene.add(object);
        });
    },
    loadObjCollada: function(obj, posX, posY, posZ) {
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;
        loader.load(obj, function(collada) {
            dae = collada.scene;
            skin = collada.skins[ 0 ];
            dae.scale.x = dae.scale.y = dae.scale.z = 0.1;
            dae.position.x = posX;
            dae.position.y = posY;
            dae.position.z = posZ;
            scene.add(dae);
        });
    },
    getFog: function() {
        scene.fog = new THREE.FogExp2(0xffffff, 0.015);
    },
    getLightObject: function() {

        //Create HemiSphere Light
        hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.6);
        hemiLight.color.setHSL(0.6, 1, 0.6);
        hemiLight.groundColor.setHSL(0.095, 1, 0.75);
        hemiLight.position.set(0, 500, 0);
        scene.add(hemiLight);

        //Create Directional Light
        dirLight = new THREE.DirectionalLight(0xffffff, 1.1);
        dirLight.color.setHSL(0.1, 1, 0.95);
        dirLight.position.set(2, 5, 3);
        dirLight.position.multiplyScalar(50);
        scene.add(dirLight);

        dirLight.castShadow = true;

        dirLight.shadowMapWidth = 2048;
        dirLight.shadowMapHeight = 2048;

        var d = 50;

        dirLight.shadowCameraLeft = -d;
        dirLight.shadowCameraRight = d;
        dirLight.shadowCameraTop = d;
        dirLight.shadowCameraBottom = -d;

        dirLight.shadowCameraFar = 3500;
        dirLight.shadowBias = -0.0001;
        dirLight.shadowDarkness = 0.35;
        //dirLight.shadowCameraVisible = true;

        //Create Sun
        var textureFlare0 = THREE.ImageUtils.loadTexture("img/lensflare0.png");
        var textureFlare2 = THREE.ImageUtils.loadTexture("img/lensflare2.png");
        var textureFlare3 = THREE.ImageUtils.loadTexture("img/lensflare3.png");

        var flareColor = new THREE.Color(0xffffff);
        flareColor.setHSL(0.55, 0.9, 0.5 + 0.5);

        var lensFlare = new THREE.LensFlare(textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor);

        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);
        lensFlare.add(textureFlare2, 512, 0.0, THREE.AdditiveBlending);

        lensFlare.add(textureFlare3, 60, 0.6, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 70, 0.7, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 120, 0.9, THREE.AdditiveBlending);
        lensFlare.add(textureFlare3, 70, 1.0, THREE.AdditiveBlending);

        lensFlare.customUpdateCallback = lensFlareUpdateCallback;
        lensFlare.position.x = 100;
        lensFlare.position.y = 380;
        lensFlare.position.z = 300;

        scene.add(lensFlare);
    }
}
function lensFlareUpdateCallback(object) {

                var f, fl = object.lensFlares.length;
                var flare;
                var vecX = -object.positionScreen.x * 2;
                var vecY = -object.positionScreen.y * 2;


                for (f = 0; f < fl; f++) {

                    flare = object.lensFlares[ f ];

                    flare.x = object.positionScreen.x + vecX * flare.distance;
                    flare.y = object.positionScreen.y + vecY * flare.distance;

                    flare.rotation = 0;

                }

                object.lensFlares[ 2 ].y += 0.025;
                object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad(45);

            }
